var game = {

    max_x: 700,
    max_y: 500,

    min_x_enemy: function () {
        return 30;
    },
    min_y_enemy: function () {
        return 30;
    },
    max_x_enemy: function () {
        return this.max_x - 10;
    },
    max_y_enemy: function () {
        return this.max_y - 60;
    },


    score: 0,
    enemy_count: 2,
    now_playing: 0,
    audio: 1,
    bonus_slot1: 0,
    bonus_slot2: 0,
    speed: 4,
    speed_user: 20,
    is_paused: 0,
    menu_level: [],
    sec_target: 0,
    undead:0,

    level_settings: {
        1: [2, 4, 15],
        2: [4, 4, 20],
        3: [7, 4, 20]
    },
    
    items : ["up", "down", "right", "left", "upright", "downright", "upleft", "downleft"],

    pause: function () {
        if (this.is_paused == 1) {
            this.is_paused = 0;
        } else {
            this.is_paused = 1;
        }
    },

    drawMenu: function () {
        if (this.is_paused == 0) {
            this.pause();
            this.menu_overlay = new PIXI.Sprite(PIXI.Texture.fromImage("graph/menu/overlay.png"));
            this.menu_overlay.position.x = 0;
            this.menu_overlay.position.y = 0;
            stage.addChildAt(this.menu_overlay, 6);

            this.game_logo = new PIXI.Sprite(PIXI.Texture.fromImage("graph/menu/game_logo.png"));
            this.game_logo.position.x = this.max_x / 3.5;
            this.game_logo.position.y = 20;
            stage.addChildAt(this.game_logo, 7);
            j = 7;
            for (i = 1; i <= 5; i++) {
                this.menu_level[i] = new PIXI.Sprite(PIXI.Texture.fromImage("graph/menu/level_" + i + ".png"));
                this.menu_level[i].position.x = (this.max_x / 6) + (64 * i);
                this.menu_level[i].position.y = 120;
                this.menu_level[i].interactive = true;
                this.menu_level[i].level = i;
                this.menu_level[i].buttonMode = true;
                this.menu_level[i].mousedown = function (data) {
                    level = data.target.level;
                    settings = game.level_settings[level];
                    game.speed = settings[1];
                    game.removeEnemy();
                    game.addEnemy(settings[0]);
                    game.sec_target = settings[2];
                    game.hideMenu();
                    game.pause();
                };
                stage.addChildAt(this.menu_level[i], 7);
            }
        } else {
            this.hideMenu();
            this.pause();
        }
    },

    hideMenu: function () {
        stage.removeChild(this.menu_overlay);
        stage.removeChild(this.game_logo);
        for (i = 1; i <= 5; i++) {
            stage.removeChild(this.menu_level[i]);
        }
    },

    addEnemy: function (count) {
        for (i = 1; i <= count; i++) {
            this.createEnemy(i);
        }
        this.enemy_count = count;
    },

    removeEnemy: function () {
        for (i = 1; i <= this.enemy_count; i++) {
            stage.removeChild(enemy[i]);
            delete(enemy[i]);
        }
        enemy = [];
        this.enemy_count = 0;
    },

    give_bonus: function (type) {
        bonus_time = 1;
        if (type == 1) {
            bonus = new PIXI.Sprite(PIXI.Texture.fromImage("graph/bonus/heart.png"));
        } else if (type == 2) {
            bonus = new PIXI.Sprite(PIXI.Texture.fromImage("graph/bonus/star.png"));
        } else if (type == 3) {
            bonus = new PIXI.Sprite(PIXI.Texture.fromImage("graph/bonus/pistol.png"));
        }
        bonus_time = rand(5, 7);
        bonus.anchor.x = 0.5;
        bonus.anchor.y = 0.5;
        bonus.position.x = rand(10, this.max_x - 50);
        bonus.position.y = rand(10, this.max_y - 50);
        stage.addChild(bonus);
        bonus_active = 1;

        bonus_type = type;
        bonus_timer = setTimeout(function () {
            stage.removeChild(bonus);
            bonus_active = 0;
        }, (bonus_time * 1000));
    },

    showBonusIcon: function (bonus_type) {
        if (bonus_type == 1) {
            this.bonus_ico = new PIXI.Sprite(PIXI.Texture.fromImage("graph/bonus/heart.png"));
            if (game.bonus_slot1 == 0) {
                this.bonus_ico.position.x = this.max_x - 48;
            } else {
                this.bonus_ico.position.x = this.max_x - 98;
            }
            this.bonus_ico.position.y = this.max_y - 35;
            stage.addChild(this.bonus_ico);
        }
    },

    drawDashborad: function () {
        this.dashboard = new PIXI.Sprite(PIXI.Texture.fromImage("graph/dashboard/dash.png"));
        this.dashboard.position.x = 0;
        this.dashboard.position.y = this.max_y - 40;

        stage.addChildAt(this.dashboard, 1);

        this.dashboard_menu = new PIXI.Sprite(PIXI.Texture.fromImage("graph/dashboard/menu.png"));
        this.dashboard_menu.interactive = true;
        this.dashboard_menu.mousedown = function () {
            game.drawMenu();
        };
        this.dashboard_menu.buttonMode = true;
        this.dashboard_menu.position.x = 476;
        this.dashboard_menu.position.y = 463;
        stage.addChildAt(this.dashboard_menu, 2);
    },

    drawScore: function () {
        score = this.score;
        if (typeof bitmapFontText !== "undefined") {
            bitmapFontText.setText("Уровень: " + score);
        } else {
            bitmapFontText = new PIXI.Text("Уровень: " + score, {
                font: "25px Tetra",
                align: "right",
                fill: "white",
            });
            bitmapFontText.position.x = 300;
            bitmapFontText.position.y = this.max_y - 30;
            stage.addChild(bitmapFontText);
        }
    },

    drawTime: function () {
        score = this.timer;
        if (typeof timerText !== "undefined") {
            timerText.setText(score + " сек.");
        } else {
            timerText = new PIXI.Text(score + " сек.", {
                font: "25px Tetra",
                align: "right",
                fill: "white",
            });
            timerText.position.x = 150;
            timerText.position.y = this.max_y - 30;
            stage.addChild(timerText);
        }
    },

    drawEnemyCounter: function () {
        c = (this.enemy_count + 1);
        c = enemy.length;
        if (typeof EnemyCounter !== "undefined") {
            EnemyCounter.setText("Врагов: " + c);
        } else {
            EnemyCounter = new PIXI.Text("Врагов: " + c, {
                font: "25px Tetra",
                align: "right",
                fill: "white",
            });
            EnemyCounter.position.x = 10;
            EnemyCounter.position.y = this.max_y - 30;
            stage.addChild(EnemyCounter);
        }
    },

    isIntersecting: function (r1, r2) {
        return !(r2.x > (r1.x + r1.width) ||
            (r2.x + r2.width) < r1.x ||
            r2.y > (r1.y + r1.height) ||
            (r2.y + r2.height) < r1.y);
    },

    setRandEnemyDirection: function (i) {
        nd = this.items[Math.floor(Math.random() * this.items.length)];
        while (nd == last_enemy_dir[i] || nd == last2_enemy_dir[i]) {
            nd = this.items[Math.floor(Math.random() * this.items.length)];
        }
        last2_enemy_dir[i] = last_enemy_dir[i];
        last_enemy_dir[i] = enemy_dir[i];
        enemy_dir[i] = nd;
    },

    createEnemy: function (i) {
        enemy[i] = new PIXI.Sprite(texture);
        enemy[i].anchor.x = 0.5;
        enemy[i].anchor.y = 0.5;
        enemy[i].position.x = 10;
        enemy[i].position.y = 10;
        stage.addChildAt(enemy[i], 2);
        this.setRandEnemyDirection(i);
    },

    process: function () {
        if (this.is_paused == 0) {
            this.timer += 1;
            this.drawTime();
            if (this.timer % 10 == 0 || this.timer == game.sec_target) {
                this.score += Math.round(this.timer / 10);
                this.enemy_count++;
                enemy[this.enemy_count] = new PIXI.Sprite(texture);
                enemy[this.enemy_count].anchor.x = 0.5;
                enemy[this.enemy_count].anchor.y = 0.5;
                enemy[this.enemy_count].position.x = 10;
                enemy[this.enemy_count].position.y = 10;
                stage.addChild(enemy[this.enemy_count]);
                this.setRandEnemyDirection(this.enemy_count);
            }
            this.drawScore();
            this.drawEnemyCounter();
        }
    },

    playSound: function (type, loop) {
        var mySound = new buzz.sound("sounds/" + type, {
            formats: ["wav"]
        });
        if (loop == 1) {

            mySound.play().loop();
        } else {

            if (this.now_playing == 0) {
                if (game.audio == 1) mySound.play();
                this.now_playing = 1;
                setTimeout(function () {
                    game.now_playing = 0;
                }, 500);
            }

        }
    },
    
    m0:8,m1:0,m2:4,m3:12,
    
    doGameAnimation: function (last_x,last_y){
        if(bunny.position.x>last_x){
            bunny.setTexture(PIXI.Texture.fromImage("graph/hero/0/"+this.m0+".png"));
            if(this.m0==11){this.m0=8;}else{this.m0++;}
        } else if(bunny.position.y>last_y){
            bunny.setTexture(PIXI.Texture.fromImage("graph/hero/0/"+this.m1+".png"));
            if(this.m1==3){this.m1=0;}else{this.m1++;}
        } else if(bunny.position.x<last_x){
            bunny.setTexture(PIXI.Texture.fromImage("graph/hero/0/"+this.m2+".png"));
            if(this.m2==7){this.m2=4;}else{this.m2++;}
        } else if(bunny.position.y<last_y){
            bunny.setTexture(PIXI.Texture.fromImage("graph/hero/0/"+this.m3+".png"));
            if(this.m3==15){this.m3=15;}else{this.m3++;}
        }
    },
    
    now_run:0,
    doDeadAnimation: function(){
        
        if(this.now_run==0){
            this.now_run=1;
            var cur=0; var i=1;
            game.inte=setInterval(function(){
                if(cur==0){bunny.tint=0xF;cur=1;}else{bunny.tint=0xFFFFFF;cur=0;}
                if(i==16){clearInterval(game.inte);game.now_run=0;game.undead=0;}else{i+=1;game.undead=1;}
            },100);
        }
    },
    
    goTo: function (input, i) {
        x = enemy[i].x;
        y = enemy[i].y;
        if (this.is_paused == 0) {
            if (typeof input == "string") {
                input = [input];
            }
            for (is in input) {
                dir = input[is];
                r=rand(1, 40);
                if (dir == "up") {
                    if (y < this.min_y_enemy()) {
                        this.setRandEnemyDirection(i);
                    } else {
                        if (r == 5) { this.setRandEnemyDirection(i);} else { enemy[i].y -= this.speed;}
                    }
                } else if (dir == "down") {
                    if (y > this.max_y_enemy()) {
                        this.setRandEnemyDirection(i);
                    } else {
                        if (r == 5) { this.setRandEnemyDirection(i);} else { enemy[i].y += this.speed;}
                    }
                } else if (dir == "right") {
                    if (x > this.max_x_enemy()) {
                        this.setRandEnemyDirection(i);
                    } else {
                        if (r == 5) { this.setRandEnemyDirection(i);} else { enemy[i].x += this.speed;}
                    }
                } else if (dir == "left") {
                    if (x < this.min_x_enemy()) {
                        this.setRandEnemyDirection(i);
                    } else {
                        if (r == 5) { this.setRandEnemyDirection(i);} else { enemy[i].x -= this.speed; }
                    }
                } else if (dir == "upleft") {
                    if (y < this.min_y_enemy() || x < this.min_x_enemy()) {
                        this.setRandEnemyDirection(i);
                    } else {
                        if (r == 5) { this.setRandEnemyDirection(i);} else { enemy[i].x -= this.speed;enemy[i].y -= this.speed; }
                    }
                } else if (dir == "upright") {
                    if (y < this.min_y_enemy() || x > this.max_x_enemy()) {
                        this.setRandEnemyDirection(i);
                    } else {
                        if (r == 5) { this.setRandEnemyDirection(i);} else { enemy[i].x += this.speed;enemy[i].y -= this.speed; }
                    }
                } else if (dir == "downright") {
                    if (y > this.max_y_enemy() || x > this.max_x_enemy()) {
                        this.setRandEnemyDirection(i);
                    } else {
                        if (r == 5) { this.setRandEnemyDirection(i);} else { enemy[i].x += this.speed;enemy[i].y += this.speed; }
                    }
                } else if (dir == "downleft") {
                    if (y > this.max_y_enemy() || x < this.min_x_enemy()) {
                        this.setRandEnemyDirection(i);
                    } else {
                        if (r == 5) { this.setRandEnemyDirection(i);} else { enemy[i].x -= this.speed;enemy[i].y += this.speed; }
                    }
                }
                
            }
        }
    },
};

function rand(min, max) {
    var argc = arguments.length;
    if (argc === 0) {
        min = 0;
        max = 2147483647;
    }
    return Math.floor(Math.random() * (max - min + 1)) + min;
}